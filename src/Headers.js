/* eslint-disable prettier/prettier */
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Header = props => {
  return (
    <View style={styles.view}>
      <Text style={styles.text}>WhatApp</Text>
      <Text style={{left: 310, bottom: 35, fontSize: 20, color: 'white'}}>
        Q
      </Text>
      <Text style={{left: 350, bottom: 63, fontSize: 20, color: 'white'}}>
        :
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    width: '100%',
    height: 150,
    backgroundColor: '#00563F',
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#ffffff',
    textAlign: 'left',
    margin: 10,
  },
});

export default Header;
