/* eslint-disable prettier/prettier */
import React from 'react';
import { Icon } from 'react-native-elements'
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  ImageDetails
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

const DATA = [
  {
    title: 'NAME',
  },
  {
    title: 'NAME',
  },
  {
    title: 'NAME',
  },
  {
    title: 'NAME',
  },
  {
    title: 'NAME',
  },
  {
    title: 'NAME',
  },
  {
    title: 'NAME',
  },
  {
    title: 'NAME',
  },
];

const Item = ({title}) => (
  <View style={styles.item}>
    <Text style={{backgroundColor:"black", borderRadius:50,width:50,height:50}}>.</Text>
    <Text style={styles.title}>{title}</Text>
    <Text style={{color:"grey",textAlign:"right",bottom:60}}>8:21 pm</Text>
  </View>
);

const ScreenA = () => {
  const renderItem = ({item}) => <Item title={item.title} />;

  return (
    <><SafeAreaView style={styles.container}>
      <ScrollView>
        <FlatList
          data={DATA}
          renderItem={renderItem}
          keyExtractor={item => item.id} />
      </ScrollView>
    </SafeAreaView></>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 8,
  },
  title: {
    fontSize: 15,
    fontWeight: '700',
    marginLeft:60,
    bottom:35,
  },
});

export default ScreenA;
