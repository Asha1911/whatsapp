/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ScreenA from './ScreenA';
import ScreenB from './ScreenB';
// import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ScreenC from './ScreenC';
import Headers from './Headers';

// const Tab = createBottomTabNavigator();
// const Tab = createMaterialBottomTabNavigator();
const Tab = createMaterialTopTabNavigator();

function App() {
  return (
    <><NavigationContainer>
    <Headers />
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, size, color }) => {
            let iconName;
            if (route.name === 'Screen_A') {
              size = focused ? 25 : 20;
              // color = focused ? '#f0f' : '#555';
            } else if (route.name === 'Screen_B') {
              size = focused ? 25 : 20;
              // color = focused ? '#f0f' : '#555';
            } else if (route.name === 'Screen_C') {
              size = focused ? 25 : 20;
              // color = focused ? '#f0f' : '#555';
            }
            // return (
            //   <FontAwesome5
            //     name={iconName}
            //     size={size}
            //     color={color} />
            // );
          },
        })}
        tabBarOptions={{
          activeTintColor: '#00563F',
          inactiveTintColor: '#00563F',
          activeBackgroundColor: '00563F',
          inactiveBackgroundColor: '00563F',
          backgroundColor:'black',
          showLabel: true,
          labelStyle: { fontSize: 15,fontWeight:'bold' },
          showIcon: true,
        }}
        activeColor="#00563F"
        inactiveColor="black"
        barStyle={{ backgroundColor: '#00563F' }}
      >
        <Tab.Screen
          name="Chats"
          component={ScreenA}
          // options={{ barStyle: 3 }}
         />
        <Tab.Screen
          name="Status"
          component={ScreenB} />
        <Tab.Screen
          name="Calls"
          component={ScreenC} />
      </Tab.Navigator>
    </NavigationContainer></>
  );
}

export default App;
